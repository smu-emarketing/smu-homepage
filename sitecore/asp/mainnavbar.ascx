<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MainNavbar.ascx.cs" Inherits="SMU_MainSublayout.layouts.ProjectX.sublayouts.Main.MainNavbar" ClientIDMode="Static"%>
		<!-- Can this be a conditional block? If nothing is binded to this placeholder do not render the markup for secondary-nav -->
<nav id="secondary-nav">
    <div class="header-container">
        <div class="header-row">
        <!-- Placeholder for secondary navigation area widgets -->
            <asp:placeholder runat="server" id="ph_SecNav"></asp:placeholder>
        </div>
    </div>
</nav>

<nav id="main-nav">
    <div class="header-container">
        <div class="header-row">
        <!--
            The area below is intended for mobile content, a call to action menu might not be needed on all
            pages so an RTE field should be used here.
            ****************************************************************************************************************
            Template Field: Mobile CTA HTML - RTE field for rendering the mobile call to action content (home page template)
            ****************************************************************************************************************
        -->
        <!-- ********* place main nav here ********* -->
            <asp:placeholder runat="server" id="ph_PrimNav"></asp:placeholder>
        </div>
    </div>
</nav>
