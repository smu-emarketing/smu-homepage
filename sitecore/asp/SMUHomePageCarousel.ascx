<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SMUHomepageCarousel.ascx.cs" Inherits="SMUHomepageCarousel.layouts.ProjectX.sublayouts.SMUHomepageCarousel" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>


<div id="full-width-carousel" class="carousel slide" data-ride="carousel">


  <ol class="carousel-indicators">
      <%= Indicator_List_Items %>
  </ol>

  <div class="carousel-inner" role="listbox">

      <%= Carousel_Items %>

  </div>

  <!-- carousel directional navigation -->

  <a class="left carousel-control" role="button" href="#full-width-carousel" role="button" data-slide="prev" aria-hidden="true">
    <span class="fa fa-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>

  <a class="right carousel-control" href="#full-width-carousel" role="button" data-slide="next" aria-hidden="true">
    <span class="fa fa-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>

</div>
