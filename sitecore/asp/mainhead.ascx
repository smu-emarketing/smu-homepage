<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MainHead.ascx.cs" Inherits="SMU_MainHead.layouts.ProjectX.sublayouts.Main.MainHead" %>
		<title>
    <asp:placeholder runat="server" id="titlePanel"></asp:placeholder>
</title>
<asp:placeholder runat="server" id="metaPanel"></asp:placeholder>
<asp:placeholder runat="server" id="headhtmlPanel"></asp:placeholder>
<asp:placeholder runat="server" id="staticCSSPanel"></asp:placeholder>
<asp:placeholder runat="server" id="googAnalyticsPanel"></asp:placeholder>
