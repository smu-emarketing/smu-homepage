<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MainBodySublayout.ascx.cs" Inherits="SMU_MainSublayout.layouts.ProjectX.sublayouts.Main.MainBodySublayout" ClientIDMode="Static"%>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
		<!--TOP -->

    <sc:placeholder runat="server" id="ph_Message" key="MessageX"></sc:placeholder>

    <header id="site-header" role="banner">

    <nav id="secondary-nav">
    <div class="header-container">
        <div class="header-row">
            <sc:placeholder runat="server" id="ph_secnavbar" key="SecondaryNavbarX"></sc:placeholder>
        </div>
    </div>
    </nav>

    <div id="masthead">
    <div class="header-container">
        <div class="header-row">
        <sc:placeholder runat="server" id="ph_masthead" key="MastheadX"></sc:placeholder>
        </div>
    </div>
    </div>
    <nav id="main-nav">
    <div class="header-container">
        <div class="header-row">

            <sc:placeholder runat="server" id="ph_primnavbar" key="PrimaryNavbarX"></sc:placeholder>
        </div>
    </div>
    </nav>
    </header>
    <!-- end of the header -->
    <div id="main-content" role="main">

        <sc:placeholder runat="server" id="ph_OpenContainer" key="OpenContainerX"></sc:placeholder>
        <div class="content-container">
            <div class="content-row">
                <sc:placeholder runat="server" id="ph_OneColumn" key="OneColumn"></sc:placeholder>
            </div>
        </div>

        <sc:placeholder runat="server" id="ph_CloseContainer" key="CloseContainerX"></sc:placeholder>
    </div>
    <!-- end the main content area -->
    <sc:placeholder runat="server" id="ph_footer" key="FooterX"></sc:placeholder>
