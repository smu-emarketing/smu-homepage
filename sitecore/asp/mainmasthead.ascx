<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MainMasthead.ascx.cs" Inherits="SMU_MainMasthead.layouts.ProjectX.sublayouts.Main.MainMasthead" %>
		<div id="site-brand">
        <div class="site-title">
            <asp:placeholder runat="server" id="desktoplogoPanel"></asp:placeholder>
            <asp:placeholder runat="server" id="mobilelogoPanel"></asp:placeholder>
        </div>
    </div>
    <div class="masthead-content">
        <asp:placeholder runat="server" id="mastheadcontentPanel"></asp:placeholder>
    </div>
