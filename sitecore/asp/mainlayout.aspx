<%@ Page language="C#" Codepage="65001" AutoEventWireup="true"  Inherits="SitecoreWebsite.layouts.ProjectX.MainLayout" CodeFile="~/layouts/ProjectX/MainLayout.aspx.cs" %>

<!DOCTYPE html>

<html lang="en">
<head runat="server">
    <meta charset="utf-8">
    <sc:placeholder runat="server" id="ph_head" key="HeadX"></sc:placeholder>
</head>

<body id="PageBody" runat="server">
  <span class="sr-only"><a href="#main-content">Skip to main content</a>.</span>
		<sc:placeholder runat="server" id="ph_openbody" key="OpenBodyX">
		</sc:placeholder>

    <form method="post" runat="server" id="mainform">

        <sc:placeholder runat="server" id="ph_premain" key="PreMainX"></sc:placeholder>

        <div id="container">
            <sc:placeholder runat="server" id="ph_maincontent" key="main-content"></sc:placeholder>

        </div>
        <!-- end main container area -->
    </form>
     <sc:placeholder runat="server" id="ph_closebody" key="CloseBodyX"></sc:placeholder></body>
</html>
