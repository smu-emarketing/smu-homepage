var gulp        = require('gulp'),
    browserSync = require('browser-sync').create(),
    sass        = require('gulp-sass'),
    access      = require('gulp-accessibility'),
    rename      = require("gulp-rename");

    gulp.task('phantom', function(){
      gulp.src("./phantom/*.js")
        .pipe(phantom({
          ext: 'json',

        }))
        .pipe(gulp.dest("./data/"));
    });

// Static Server
gulp.task('serve', ['sass'], function() {

    browserSync.init({
        server: "./public",
        notify: false
    });

    gulp.watch("./src/scss/**/*.scss", ['sass']);
    gulp.watch("./public/*.html").on("change", browserSync.reload);

});

// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', function() {

    return gulp.src("./src/scss/**/**/*.scss")
        .pipe(sass({outputStyle: 'compact', sourceComments: 'map'}))
        //.pipe(sass({outputStyle: 'compact'}))
        .pipe(gulp.dest("./public/css"))
        .pipe(browserSync.stream());

});

gulp.task('test', function() {
  return gulp.src('./public/**/*.html')
    .pipe(access({
      force: true
    }))
    .on('error', console.log)
    .pipe(access.report({reportType: 'csv'}))
    .pipe(rename({
      extname: '.csv'
    }))
    .pipe(gulp.dest('reports/csv'));
});

gulp.task('default', ['serve']);
